package pl.szafa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SzafaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SzafaApplication.class, args);
	}

}
