package pl.szafa.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "okrycia")
@JsonAutoDetect
public class Okrycie {

    public Okrycie() {
    }

    public Okrycie(long ID, String kolor, String typ, String rok, String rodzaj, String path) {
        this.ID = ID;
        this.kolor = kolor;
        this.typ = typ;
        this.rok = rok;
        this.rodzaj = rodzaj;
        this.path = path;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long ID;

    @Column(name = "KOLOR")
    private String kolor;

    @Column(name = "TYP")
    private String typ;

    @Column(name = "ROK")
    private String rok;

    @Column(name = "RODZAJ")
    private String rodzaj;

    @Column(name = "IMG_PATH")
    private String path;

    @Override
    public String toString() {
        return "Okrycie{" +
                "ID=" + ID +
                ", kolor='" + kolor + '\'' +
                ", typ='" + typ + '\'' +
                ", rok='" + rok + '\'' +
                ", rodzaj='" + rodzaj + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getRok() {
        return rok;
    }

    public void setRok(String rok) {
        this.rok = rok;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
