package pl.szafa.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "bluzki")
@JsonAutoDetect
public class Bluzka {

    public Bluzka() {
    }

    public Bluzka(long ID, String kolor, String typ, String rekaw, String rok, String path) {
        this.ID = ID;
        this.kolor = kolor;
        this.typ = typ;
        this.rekaw = rekaw;
        this.rok = rok;
        this.path = path;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long ID;

    @Column(name = "KOLOR")
    private String kolor;

    @Column(name = "TYP")
    private String typ;

    @Column(name = "REKAW")
    private String rekaw;

    @Column(name = "ROK")
    private String rok;

    @Column(name = "IMG_PATH")
    private String path;

    @Override
    public String toString() {
        return "Bluzka{" +
                "ID=" + ID +
                ", kolor='" + kolor + '\'' +
                ", typ='" + typ + '\'' +
                ", rekaw='" + rekaw + '\'' +
                ", rok='" + rok + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getRekaw() {
        return rekaw;
    }

    public void setRekaw(String rekaw) {
        this.rekaw = rekaw;
    }

    public String getRok() {
        return rok;
    }

    public void setRok(String rok) {
        this.rok = rok;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
