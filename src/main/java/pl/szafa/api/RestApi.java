package pl.szafa.api;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.szafa.dao.DbController;
import pl.szafa.model.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("")
public class RestApi {

    private static long imgId = 0;

    @GetMapping("/help")
    public String help() {
        return "Api works!";
    }

    @PostMapping("/addBluzka")
    public Bluzka addBluzka(@RequestBody Bluzka bluzka) {
        DbController dbController = new DbController();
        bluzka.setPath("img_" + DbController.getImgId());

        return (Bluzka) dbController.addToDataBase(bluzka);
    }

    @PostMapping("/addButy")
    public Buty addButy(@RequestBody Buty buty) {
        DbController dbController = new DbController();
        buty.setPath("img_" + DbController.getImgId());

        return (Buty) dbController.addToDataBase(buty);
    }

    @PostMapping("/addDol")
    public Dol addDol(@RequestBody Dol dol) {
        DbController dbController = new DbController();
        dol.setPath("img_" + DbController.getImgId());
        return (Dol) dbController.addToDataBase(dol);
    }

    @PostMapping("/addOkrycie")
    public Okrycie addOkrycie(@RequestBody Okrycie okrycie) {
        DbController dbController = new DbController();
        okrycie.setPath("img_" + DbController.getImgId());
        return (Okrycie) dbController.addToDataBase(okrycie);
    }

    @PostMapping("/addSukienka")
    public Sukienka addSukienka(@RequestBody Sukienka sukienka) {
        DbController dbController = new DbController();
        sukienka.setPath("img_" + DbController.getImgId());
        return (Sukienka) dbController.addToDataBase(sukienka);
    }

    @PostMapping("/addTorebka")
    public Torebka addTorebka(@RequestBody Torebka torebka) {
        DbController dbController = new DbController();
        torebka.setPath("img_" + DbController.getImgId());
        return (Torebka) dbController.addToDataBase(torebka);
    }

    @PostMapping("/getBluzka")
    public List<Bluzka> getBluzka(@RequestBody Bluzka bluzka) {
        DbController dbController = new DbController();
        return dbController.getBluzki(bluzka);
    }

    @PostMapping("/getButy")
    public List<Buty> getButy(@RequestBody Buty buty) {
        DbController dbController = new DbController();
        return dbController.getButy(buty);
    }

    @PostMapping("/getDol")
    public List<Dol> getDol(@RequestBody Dol dol) {
        DbController dbController = new DbController();
        return dbController.getDol(dol);
    }

    @PostMapping("/getOkrycie")
    public List<Okrycie> getOkrycie(@RequestBody Okrycie okrycie) {
        DbController dbController = new DbController();
        return dbController.getOkrycie(okrycie);
    }

    @PostMapping("/getSukienka")
    public List<Sukienka> getButy(@RequestBody Sukienka sukienka) {
        DbController dbController = new DbController();
        return dbController.getSukienka(sukienka);
    }


    @PostMapping("/getTorebka")
    public List<Torebka> getTorebka(@RequestBody Torebka torebka) {
        DbController dbController = new DbController();
        return dbController.getTorebka(torebka);
    }

    @PostMapping("/addImage")
    public Map<String, String> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("path") String path) {
        path = "./images/" + path + ".jpg";
        DbController dbController = new DbController();
        if(dbController.saveImage(file, path)) {
            Map<String, String> response = new HashMap<>();
            response.put("ans", "OK");
            return response;
        } else {
            Map<String, String> response = new HashMap<>();
            response.put("ans", "Err");
            return response;
        }
    }

    @RequestMapping(value = "/getImage/{imgId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getImage(@PathVariable String imgId) {
        try {
            InputStream in = new FileInputStream("./images/" + imgId + ".jpg");
            return IOUtils.toByteArray(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
