package pl.szafa.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.web.multipart.MultipartFile;
import pl.szafa.model.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;

public class DbController {
    private Session session;
    private Transaction transaction;
    public static int imgId = getLasId();

    public DbController() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    private static int getLasId() {
        File folder = new File("./images");
        File[] listFiles = folder.listFiles();

        if(listFiles.length > 0) {
            String lastFile = listFiles[listFiles.length - 1].getName();
            lastFile = lastFile.replace("img_", "");
            lastFile = lastFile.replace(".jpg", "");
            int lastId = Integer.parseInt(lastFile);

            System.out.println(lastId);

            return lastId;
        } else {
            return 0;
        }
    }

    public static int getImgId() {
        imgId++;
        return imgId;
    }

    public Object addToDataBase(Object o) {
        transaction = session.beginTransaction();

        try {
            session.save(o);

            session.flush();
            transaction.commit();

            return o;
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }

        return null;
    }

    public List<Bluzka> getBluzki(Bluzka bluzka) {
        try {
            String params = "";
            if(!bluzka.getKolor().equals("")) {
                params += "kolor = :kolor";
            }
            if(!bluzka.getTyp().equals("")) {
                params += " AND typ = :typ";
            }
            if(!bluzka.getRekaw().equals("")) {
                params += " AND rekaw = :rekaw";
            }
            if(!bluzka.getRok().equals("")) {
                params += " AND rok = :rok";
            }
            if(params.startsWith(" AND")) {
                params = params.replaceFirst(" AND", "");
            }
            if(!params.equals("")) {
                params = " where " + params;
            }

            List<Bluzka> results = (List<Bluzka>) session.createQuery("from Bluzka" + params)
                    .setProperties(bluzka)
                    .list();

            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Buty> getButy(Buty buty) {

        String params = "";
        if(!buty.getKolor().equals("")) {
            params += "kolor = :kolor";
        }
        if(!buty.getTyp().equals("")) {
            params += " AND typ = :typ";
        }
        if(!buty.getRok().equals("")) {
            params += " AND rok = :rok";
        }
        if(params.startsWith(" AND")) {
            params = params.replaceFirst(" AND", "");
        }
        if(!params.equals("")) {
            params = " where " + params;
        }

        List<Buty> results = (List<Buty>) session.createQuery("from Buty" + params)
                .setProperties(buty)
                .list();

        return results;
    }

    public List<Dol> getDol(Dol dol) {

        String params = "";
        if(!dol.getKolor().equals("")) {
            params += "kolor = :kolor";
        }
        if(!dol.getTyp().equals("")) {
            params += " AND typ = :typ";
        }
        if(!dol.getRodzaj().equals("")) {
            params += " AND rodzaj = :rodzaj";
        }
        if(!dol.getRok().equals("")) {
            params += " AND rok = :rok";
        }
        if(params.startsWith(" AND")) {
            params = params.replaceFirst(" AND", "");
        }
        if(!params.equals("")) {
            params = " where " + params;
        }

        List<Dol> results = (List<Dol>) session.createQuery("from Dol" + params)
                .setProperties(dol)
                .list();
        return results;
    }

    public List<Okrycie> getOkrycie(Okrycie okrycie) {
        String params = "";
        if(!okrycie.getKolor().equals("")) {
            params += "kolor = :kolor";
        }
        if(!okrycie.getTyp().equals("")) {
            params += " AND typ = :typ";
        }
        if(!okrycie.getRodzaj().equals("")) {
            params += " AND rodzaj = :rodzaj";
        }
        if(!okrycie.getRok().equals("")) {
            params += " AND rok = :rok";
        }
        if(params.startsWith(" AND")) {
            params = params.replaceFirst(" AND", "");
        }
        if(!params.equals("")) {
            params = " where " + params;
        }

        List<Okrycie> results = (List<Okrycie>) session.createQuery("from Okrycie" + params)
                .setProperties(okrycie)
                .list();
        return results;
    }

    public List<Sukienka> getSukienka(Sukienka sukienka) {
        String params = "";
        if(!sukienka.getKolor().equals("")) {
            params += "kolor = :kolor";
        }
        if(!sukienka.getTyp().equals("")) {
            params += " AND typ = :typ";
        }
        if(!sukienka.getRok().equals("")) {
            params += " AND rok = :rok";
        }
        if(params.startsWith(" AND")) {
            params = params.replaceFirst(" AND", "");
        }
        if(!params.equals("")) {
            params = " where " + params;
        }

        List<Sukienka> results = (List<Sukienka>) session.createQuery("from Sukienka" + params)
                .setProperties(sukienka)
                .list();
        return results;
    }

    public List<Torebka> getTorebka(Torebka torebka) {
        String params = "";
        if(!torebka.getKolor().equals("")) {
            params += "kolor = :kolor";
        }
        if(!torebka.getTyp().equals("")) {
            params += " AND typ = :typ";
        }
        if(!torebka.getRok().equals("")) {
            params += " AND rok = :rok";
        }
        if(params.startsWith(" AND")) {
            params = params.replaceFirst(" AND", "");
        }
        if(!params.equals("")) {
            params = " where " + params;
        }

        List<Torebka> results = (List<Torebka>) session.createQuery("from Torebka" + params)
                .setProperties(torebka)
                .list();
        return results;
    }

    public boolean saveImage(MultipartFile file, String path) {
        try {
            if (!file.isEmpty()) {
                BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
                File destination = new File(path);
                ImageIO.write(src, "png", destination);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
